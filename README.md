We've all seen in the news that "So-and-so was released on bail of $50,000." What does this actually mean? What exactly is bail? What are the differences between it and the Bail Bond?

If someone is detained for an offense, they will be detained until their court date, or until an judge makes a decision to release them in their personal capacity and/or until granted bail.

How do you define Bail?
Bail is a specific amount of money which acts as a security measure between the court and the person who is in the jail (the person who is the accused.) The defendant has the option of pay their bail cash, but a lot of them are not able to pay it in cash.

Because bail is usually set at a very high amount, many defendants cannot afford to pay bail on their own. They require assistance by a bail agent as well as a Bail Bondsman who will post bail Bond to them.

What's a Bail Bond?</br>

[Bail Bonds in Oakland, CA](https://g.page/all-pro-oakland?share) is a kind of security bond that is provided by a certain bond company via bail agents or Bail Bondsman to secure the release of the defendant from prison. There are two kinds of Bail Bonds:

Criminal Bail Bond: utilized in criminal cases and guarantees that the defendant will is present for trial when summoned by the judge and also assures payment of any penalties or fines which are imposed on behalf of the accused.

Civil Bail Bond is used in civil proceedings and to guarantee the repayment of the debt plus cost and interest, if imposed on the person who is the defendant.

How do Bail Bonds Work?
A judge will set the bail amount. If the defendant is unable to afford the amount of bail on themselves, they can solicit assistance from an Bail bondsman by way of an Bail Bond.

In order to post the Bail Bond the defendant must pay an Bail bondsman 10 percent of the bail amount.

The Bail Bondsman will guarantee the remaining bail amount through collateral. If the defendant doesn't have sufficient collateral to cover the bail, the Bail Bondsman could ask friends and relatives to assist in paying the bail.

Sometimes, an additional cash deposit and the entire collateral is required in order to enable the Bail Bond to be posted.

What happens next is contingent on whether the defendant is present in court following his release.

If the defendant does not show up in the courtroom:The Bail Bond is forfeited, and the court will require that the rest of bail amount to be paid. The Bail Bondsman will make use of the collateral of the defendant (house or jewelry, stocks and so on) as collateral to cover the remainder of the bail amount.

When a person who is a defendant doesn't show up for the court date:Upon conclusion of the trial there is no bail bond. Bail Bond is dissolved and the collateral is returned to the person who deposited it. The Bail Bondsman retains the 10% cash amount as a profit.
Bail Bond Example
For instance, John is arrested. The court sets his bail to $10,000. John is looking to get out of prison however, he's not able to pay $10,000 cash on hand So he asks for assistance from the Bail bondsman to put up an Bail Bond for John.

The bondsman must have $1,000 in order to issue an Bail Bond for John, so that he is released from the prison.

In addition to the $9,000 bail amount, the bondman obtains security from John or John's family. Collateral can take the form of a car jewellery, a house or other items.

If John attends all court dates and is present, the Bail Bondsman is not required to pay funds or collateral. The Bail Bond will be dissolved following the conclusion of John's trial. John's will get his $9,000 collateral back however he will not receive the $1,000; the bondsman would retain this as a profit.

If John doesn't show up in court the bondsman will have to pay the court the remainder of $9,000 bail. To accomplish that, the bondman will utilize John's collateral.

If John had paid his $110,000 with cash and received the money in cash, he would have a right to a reimbursement after the end of the trial no matter what the decision.
